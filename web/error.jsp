<%-- 
    Document   : error
    Created on : 14/06/2024, 3:29:51 p. m.
    Author     : driss
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio Sesion</title>
    <link rel="stylesheet" href="style/index.css">
</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="logo.jpeg">
        </div>
        <h2>INICIAR SESION</h2>
        <form method="post"  action="principal" autocomplete="off" novalidate class="<%= request.getAttribute("errorExists") != null ?  "error-bg" : "" %>">
            <div class="input-group">
                <label for="username"> Nombre</label>
                <input type="text" id="username" name="username" required>
            </div>
            <div class="input-group">
                
                <label for="password"> Contraseña</label>
                <input type="password" id="passwordd" name="passwordd" required>
            </div>
           
            <div class="forgot-password">
                <a href="#">Olvidé la contraseña</a>
            </div>
            <button type="submit">Inicio sesión</button>
        </form>
        <div class="create-account">
            <a href="crear_cuenta.html">Crear cuenta</a>
        </div>
            
            <div class="input-group">
            <%
                String error  = (String) request.getAttribute("error");
            if (error != null) {            
            %>
            <p class="error"><%= error %></p>
            <%
                }
                %>
               </div> 
    </div>
                
    
</body>
</html>


